from PyQt6.uic import loadUi
from PyQt6.QtWidgets import *
from sqlshot import sqlqueryselecttbl, sqlquerydeleterec # this is connection for sqlite3

class CheckDelete:
    def __init__(self):
        self.mainui = loadUi('chkui.ui')
        self.mainui.pbDelete.clicked.connect(self.deleterows) #this for triggering function from button
        self.loaddata()                         #this is to initalize record from table to show
        self.mainui.show()
        
    def loaddata(self):
        self.mainui.tbllibrary.clear()
        column = ["Check", "Id", "Isbn", "Title", "Author", "Borrowed Time", "Borrower"]
        self.mainui.tbllibrary.setColumnCount(len(column))
        self.mainui.tbllibrary.setHorizontalHeaderLabels(column)
        
        people = sqlqueryselecttbl()         # this is selecting all record from db sqlite3
        self.mainui.tbllibrary.setRowCount(len(people) - len(people)) # to prevent generate trash row on table
        
        for person in people:
            checkbox = QCheckBox(self.mainui.tbllibrary)
            inx = people.index(person)
            self.mainui.tbllibrary.insertRow(inx)
            self.mainui.tbllibrary.setCellWidget(inx, 0, checkbox)
            
            self.mainui.tbllibrary.setItem(inx, 1, QTableWidgetItem( str(person[0]) ))
            self.mainui.tbllibrary.setItem(inx, 2, QTableWidgetItem( str(person[1]) ))
            self.mainui.tbllibrary.setItem(inx, 3, QTableWidgetItem( person[2] ))
            self.mainui.tbllibrary.setItem(inx, 4, QTableWidgetItem( person[3] ))
            self.mainui.tbllibrary.setItem(inx, 5, QTableWidgetItem( person[4] ))
            self.mainui.tbllibrary.setItem(inx, 6, QTableWidgetItem( person[5] ))
            self.mainui.tbllibrary.update()
            
    def deleterows(self):
        for row in range(self.mainui.tbllibrary.rowCount()):
            chk_item = self.mainui.tbllibrary.cellWidget(row, 0)
            
            if isinstance(chk_item, QCheckBox) and chk_item.isChecked():
                data = self.mainui.tbllibrary.item(row, 1).text()
                sqlquerydeleterec(data)               # to delete record from sqlite3
                
        self.loaddata()


if __name__ == '__main__':
    app = QApplication([])
    chk = CheckDelete()
    app.exec()